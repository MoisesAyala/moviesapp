package com.review.moviesapp.retrofit

import com.review.moviesapp.home.model.response.MostPopular
import com.review.moviesapp.home.model.response.PlayingNow
import com.review.moviesapp.movie.model.response.MovieDetails
import com.review.moviesapp.movie.model.response.VideosFromMovie
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("movie/now_playing")
    suspend fun callNowPlaying(@Query("api_key") key: String,
                               @Query("language") language: String): PlayingNow

    @GET("movie/popular")
    suspend fun callMostPopular(@Query("api_key") key: String,
                                @Query("language") language: String): MostPopular

    @GET("movie/{id}")
    suspend fun callMovieDetails(@Path("id") id: Int,
                                 @Query("api_key") key: String,
                                 @Query("language") language: String): MovieDetails

    @GET("movie/{id}/videos")
    suspend fun callVideosFromMovie(@Path("id") id: Int,
                                 @Query("api_key") key: String,
                                 @Query("language") language: String): VideosFromMovie
}