package com.review.moviesapp.home.extension

import com.review.moviesapp.home.model.response.MostPopular
import com.review.moviesapp.home.model.response.PlayingNow

fun PlayingNow.avoidErrorNullData(): PlayingNow {
    return PlayingNow(
        page = this.page,
        totalPages = this.totalPages,
        totalResults = this.totalResults,
        results = this.results,
        dates = this.dates
    )
}

fun MostPopular.avoidErrorNullData(): MostPopular {
    return MostPopular(
        page = this.page,
        totalPages = this.totalPages,
        totalResults = this.totalResults,
        results = this.results
    )
}