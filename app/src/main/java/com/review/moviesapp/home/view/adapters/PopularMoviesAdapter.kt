package com.review.moviesapp.home.view.adapters

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.review.moviesapp.BuildConfig
import com.review.moviesapp.application.OnItemClickListener
import com.review.moviesapp.databinding.LayoutPopularMoviesItemBinding
import com.review.moviesapp.home.model.response.MostPopular

class PopularMoviesAdapter(private val context: Context, private val moviesList: MostPopular) :
    RecyclerView.Adapter<PopularMoviesAdapter.MoviesViewHolder>() {

    private lateinit var clickListener: OnItemClickListener

    class MoviesViewHolder(val binding: LayoutPopularMoviesItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        return MoviesViewHolder(
            LayoutPopularMoviesItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return moviesList.results.size
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        val rate = moviesList.results[position].voteAverage.toString()
        with(holder.binding) {
            title.text = moviesList.results[position].title
            Glide.with(context).load(
                BuildConfig.LOAD_IMAGE_W500.plus(moviesList.results[position].posterPath)
            ).into(poster)
            progressRateBar.progress = rate.replace(".", "").toInt()
            rateQuantity.text = rate
            validateColorBar(progressRateBar, rate.take(1))

            cardContainer.setOnClickListener {
                clickListener.onItemClick(moviesList.results[position].id)
            }
        }
    }

    private fun validateColorBar(progressRateBar: ProgressBar, rate: String) {
        when (rate.toInt()) {
            10 -> progressRateBar.progressDrawable.setTintList(ColorStateList.valueOf(Color.GREEN))
            9 -> progressRateBar.progressDrawable.setTintList(ColorStateList.valueOf(Color.GREEN))
            8 -> progressRateBar.progressDrawable.setTintList(ColorStateList.valueOf(Color.GREEN))
            7 -> progressRateBar.progressDrawable.setTintList(ColorStateList.valueOf(Color.YELLOW))
            6 -> progressRateBar.progressDrawable.setTintList(ColorStateList.valueOf(Color.BLACK))
            else -> progressRateBar.progressDrawable.setTintList(ColorStateList.valueOf(Color.RED))
        }
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        clickListener = listener
    }
}