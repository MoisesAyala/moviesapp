package com.review.moviesapp.home.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.review.moviesapp.application.OnItemClickListener
import com.review.moviesapp.databinding.LayoutPlayingNowItemBinding
import com.review.moviesapp.home.model.response.PlayingNow

class PlayingNowAdapter(private val context: Context, private val moviesList: PlayingNow) :
    RecyclerView.Adapter<PlayingNowAdapter.MoviesViewHolder>() {

    private lateinit var clickListener: OnItemClickListener

    class MoviesViewHolder(val binding: LayoutPlayingNowItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        return MoviesViewHolder(
            LayoutPlayingNowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return moviesList.results.size
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        with(holder.binding) {
            title.text = moviesList.results[position].title
            Glide.with(context).load(
                "https://image.tmdb.org/t/p/w500/" +
                        moviesList.results[position].posterPath
            ).into(poster)
            datePlaying.text = moviesList.results[position].releaseDate
            cardContainer.setOnClickListener {
                clickListener.onItemClick(moviesList.results[position].id)
            }
        }
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        clickListener = listener
    }
}