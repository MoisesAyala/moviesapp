package com.review.moviesapp.home.view.fragments

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.review.moviesapp.application.IntentUtils
import com.review.moviesapp.application.OnItemClickListener
import com.review.moviesapp.databinding.FragmentPopularMoviesBinding
import com.review.moviesapp.home.model.response.MostPopular
import com.review.moviesapp.home.view.adapters.PopularMoviesAdapter
import com.review.moviesapp.extension.observe
import com.review.moviesapp.home.viewmodel.HomeViewModel

class PopularMoviesFragment : Fragment() {

    private lateinit var binding: FragmentPopularMoviesBinding
    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View = initViewBinding(inflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.callMostPopular()
        viewModel.getMostPopular.observe(viewLifecycleOwner, { popular ->
            initMostPopularAdapter(popular)
        }, { it.printStackTrace() })
    }

    private fun initViewBinding(inflater: LayoutInflater): View {
        binding = FragmentPopularMoviesBinding.inflate(inflater)
        viewModel = HomeViewModel.create(Application())
        return binding.root
    }

    private fun initMostPopularAdapter(playingNow: MostPopular) {
        val adapterMovies = PopularMoviesAdapter(requireContext(), playingNow)
        binding.popularMoviesRecycler.apply {
            adapter = adapterMovies
            layoutManager = GridLayoutManager(
                requireContext(), 2,
                GridLayoutManager.VERTICAL, false
            )
            isNestedScrollingEnabled = true
        }
        adapterMovies.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(movieId: Int) {
                IntentUtils.intentToMovieDetails(requireContext(), movieId)
            }
        })
        binding.loading.visibility = View.GONE
        binding.popularMoviesRecycler.visibility = View.VISIBLE
    }
}