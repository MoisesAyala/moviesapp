package com.review.moviesapp.home.model

import com.review.moviesapp.home.model.response.MostPopular
import com.review.moviesapp.home.model.response.PlayingNow

interface HomeRepository {
    suspend fun callNowPlaying(): PlayingNow
    suspend fun callMostPopular(): MostPopular
}