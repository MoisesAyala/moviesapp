package com.review.moviesapp.home.model.response

import com.google.gson.annotations.SerializedName
import com.review.moviesapp.home.model.Results

data class MostPopular(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: List<Results>,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("total_results") val totalResults: Int
)