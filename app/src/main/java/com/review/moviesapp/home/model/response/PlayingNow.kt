package com.review.moviesapp.home.model.response

import com.google.gson.annotations.SerializedName
import com.review.moviesapp.home.model.Dates
import com.review.moviesapp.home.model.Results

data class PlayingNow(
    @SerializedName("dates") val dates: Dates,
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: ArrayList<Results>,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("total_results") val totalResults: Int
)