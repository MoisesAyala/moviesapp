package com.review.moviesapp.home.view.fragments

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.review.moviesapp.application.IntentUtils
import com.review.moviesapp.application.OnItemClickListener
import com.review.moviesapp.databinding.FragmentPlayingNowBinding
import com.review.moviesapp.extension.observe
import com.review.moviesapp.home.model.response.PlayingNow
import com.review.moviesapp.home.view.adapters.PlayingNowAdapter
import com.review.moviesapp.home.viewmodel.HomeViewModel

class PlayingNowFragment : Fragment() {

    private lateinit var binding: FragmentPlayingNowBinding
    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View = initViewBinding(inflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.callPlayingNow()
        viewModel.getPlayingNow.observe(viewLifecycleOwner, {
            initNowPlayingAdapter(it)
        }, { it.printStackTrace() })
    }

    private fun initViewBinding(inflater: LayoutInflater): View {
        binding = FragmentPlayingNowBinding.inflate(inflater)
        viewModel = HomeViewModel.create(Application())
        return binding.root
    }

    private fun initNowPlayingAdapter(playingNow: PlayingNow) {
        val adapterMovies = PlayingNowAdapter(requireContext(), playingNow)
        binding.nowPlayingRecycler.apply {
            adapter = adapterMovies
            layoutManager = GridLayoutManager(
                requireContext(), 2,
                GridLayoutManager.VERTICAL, false
            )
            isNestedScrollingEnabled = true
        }
        adapterMovies.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(movieId: Int) {
                IntentUtils.intentToMovieDetails(requireContext(), movieId)
            }
        })
        binding.loading.visibility = View.GONE
        binding.nowPlayingRecycler.visibility = View.VISIBLE
    }
}