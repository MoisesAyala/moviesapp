package com.review.moviesapp.home.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.review.moviesapp.extension.launch
import com.review.moviesapp.extension.Result
import com.review.moviesapp.home.model.HomeRepository
import com.review.moviesapp.home.model.HomeRepositoryImpl
import com.review.moviesapp.home.model.response.MostPopular
import com.review.moviesapp.home.model.response.PlayingNow
import kotlinx.coroutines.Dispatchers

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val homeRepository: HomeRepository = HomeRepositoryImpl()

    private val _playingNowLiveData: MutableLiveData<Result<PlayingNow>> = MutableLiveData()
    private val _popularLiveData: MutableLiveData<Result<MostPopular>> = MutableLiveData()

    val getPlayingNow: LiveData<Result<PlayingNow>>
        get() = _playingNowLiveData

    val getMostPopular: LiveData<Result<MostPopular>>
        get() = _popularLiveData

    fun callPlayingNow() {
        viewModelScope.launch(_playingNowLiveData, Dispatchers.IO) {
            val result = homeRepository.callNowPlaying()
            if (result.results.isNullOrEmpty())
                throw Exception()
            result
        }
    }

    fun callMostPopular() {
        viewModelScope.launch(_popularLiveData, Dispatchers.IO) {
            val result = homeRepository.callMostPopular()
            if (result.results.isNullOrEmpty())
                throw Exception()
            result
        }
    }

    companion object {
        fun create(app: Application): HomeViewModel {
            return HomeViewModel(app)
        }
    }

}