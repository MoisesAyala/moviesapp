package com.review.moviesapp.home.model

import com.review.moviesapp.BuildConfig
import com.review.moviesapp.home.extension.avoidErrorNullData
import com.review.moviesapp.home.model.response.MostPopular
import com.review.moviesapp.home.model.response.PlayingNow
import com.review.moviesapp.retrofit.ApiAdapter
import java.util.*

class HomeRepositoryImpl : HomeRepository {

    private val apiKey = "1fb75181acbca031bbb3237794468c44"
    private val locale = "es-MX"

    override suspend fun callNowPlaying(): PlayingNow {
        val api = ApiAdapter(BuildConfig.MOVIE_DB_API)
        val result = api.clientService().callNowPlaying(apiKey, locale)
        return result.avoidErrorNullData()
    }

    override suspend fun callMostPopular(): MostPopular {
        val api = ApiAdapter(BuildConfig.MOVIE_DB_API)
        val result = api.clientService().callMostPopular(apiKey, locale)
        return result.avoidErrorNullData()
    }
}