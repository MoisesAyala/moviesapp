package com.review.moviesapp.home.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.review.moviesapp.R
import com.review.moviesapp.databinding.ActivityHomeBinding
import com.review.moviesapp.home.view.fragments.PlayingNowFragment
import com.review.moviesapp.home.view.fragments.PopularMoviesFragment

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(initViewBinding(layoutInflater))

        val arrayTabs = arrayOf(
            getString(R.string.playing_now_title),
            getString(R.string.popular_movies_title)
        )
        binding.viewPager.adapter = CheckoutPagerAdapter(supportFragmentManager)
        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.text = arrayTabs[position]
        }.attach()
    }

    private fun initViewBinding(inflater: LayoutInflater): View {
        binding = ActivityHomeBinding.inflate(inflater)
        return binding.root
    }

    inner class CheckoutPagerAdapter(fragmentManager: FragmentManager) :
        FragmentStateAdapter(fragmentManager, lifecycle) {
        private val fragmentList = arrayListOf(PlayingNowFragment(), PopularMoviesFragment())

        override fun getItemCount(): Int = fragmentList.size

        override fun createFragment(position: Int): Fragment =
            when (position) {
                0 -> PlayingNowFragment()
                else -> PopularMoviesFragment()
            }
    }

}