package com.review.moviesapp.application

import android.content.Context
import android.content.Intent
import com.review.moviesapp.movie.view.MovieDetailsActivity

object IntentUtils {

    fun intentToMovieDetails(context: Context, movieId: Int) {
        val intent = Intent(context, MovieDetailsActivity::class.java)
        intent.putExtra("movieId", movieId)
        context.startActivity(intent)
    }
}