package com.review.moviesapp.application

interface OnItemClickListener {
    fun onItemClick(movieId: Int)
}