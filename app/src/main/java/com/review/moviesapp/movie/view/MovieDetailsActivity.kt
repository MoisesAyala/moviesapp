package com.review.moviesapp.movie.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.review.moviesapp.R
import com.review.moviesapp.databinding.ActivityMovieDetailBinding
import com.review.moviesapp.movie.view.fragments.MovieDetailsFragment

class MovieDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMovieDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(initViewBinding(layoutInflater))
        actionBar?.setDisplayHomeAsUpEnabled(true)

        if (intent.extras != null) {
            goToMovieDetailsFragment(intent.getIntExtra("movieId", 0))
        }
    }

    private fun initViewBinding(inflater: LayoutInflater): View {
        binding = ActivityMovieDetailBinding.inflate(inflater)
        return binding.root
    }

    private fun goToMovieDetailsFragment(movieId: Int) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.containerView, MovieDetailsFragment.newInstance(movieId))
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }
}