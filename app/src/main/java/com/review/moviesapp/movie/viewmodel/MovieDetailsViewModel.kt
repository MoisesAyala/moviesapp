package com.review.moviesapp.movie.viewmodel

import androidx.lifecycle.AndroidViewModel
import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.review.moviesapp.extension.launch
import com.review.moviesapp.extension.Result
import com.review.moviesapp.movie.model.MovieDetailsRepository
import com.review.moviesapp.movie.model.MovieDetailsRepositoryImpl
import com.review.moviesapp.movie.model.response.MovieDetails
import com.review.moviesapp.movie.model.response.VideosFromMovie
import kotlinx.coroutines.Dispatchers

class MovieDetailsViewModel(application: Application) : AndroidViewModel(application) {

    private val movieDetailsRepository: MovieDetailsRepository = MovieDetailsRepositoryImpl()

    private val _movieDetailsLiveData: MutableLiveData<Result<MovieDetails>> = MutableLiveData()
    private val _videosLiveData: MutableLiveData<Result<VideosFromMovie>> = MutableLiveData()

    val getMovieDetails: LiveData<Result<MovieDetails>>
        get() = _movieDetailsLiveData

    val getVideosFromMovie: LiveData<Result<VideosFromMovie>>
        get() = _videosLiveData

    fun callMovieDetails(movieId: Int) {
        viewModelScope.launch(_movieDetailsLiveData, Dispatchers.IO) {
            val result = movieDetailsRepository.callMovieDetails(movieId)
            if (result.posterPath.isEmpty())
                throw Exception()
            result
        }
    }

    fun callVideosFromMovie(movieId: Int) {
        viewModelScope.launch(_videosLiveData, Dispatchers.IO) {
            val result = movieDetailsRepository.callVideosFromMovie(movieId)
            if (result.results.isNullOrEmpty())
                throw Exception()
            result
        }
    }

    companion object {
        fun create(app: Application): MovieDetailsViewModel {
            return MovieDetailsViewModel(app)
        }
    }

}