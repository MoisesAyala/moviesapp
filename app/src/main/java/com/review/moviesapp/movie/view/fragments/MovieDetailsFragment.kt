package com.review.moviesapp.movie.view.fragments

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.review.moviesapp.databinding.FragmentMovieDetailsBinding
import com.review.moviesapp.extension.observe
import com.review.moviesapp.movie.model.Genres
import com.review.moviesapp.movie.model.response.MovieDetails
import com.review.moviesapp.movie.model.response.VideosFromMovie
import com.review.moviesapp.movie.viewmodel.MovieDetailsViewModel
import java.util.ArrayList
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.review.moviesapp.BuildConfig

class MovieDetailsFragment : Fragment() {

    private lateinit var binding: FragmentMovieDetailsBinding
    private lateinit var viewModel: MovieDetailsViewModel
    private var movieId: Int = 0

    companion object {
        fun newInstance(movieId: Int) = MovieDetailsFragment().apply {
            arguments = Bundle().apply {
                putInt("movieId", movieId)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movieId = it.getInt("movieId", 0)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View = initViewBinding(inflater)

    private fun initViewBinding(inflater: LayoutInflater): View {
        binding = FragmentMovieDetailsBinding.inflate(inflater)
        viewModel = MovieDetailsViewModel.create(Application())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.callMovieDetails(movieId)
        viewModel.callVideosFromMovie(movieId)
        viewModel.getMovieDetails.observe(viewLifecycleOwner, {
            initViewDetailed(it)
        }, {
            it.printStackTrace()
        })

        viewModel.getVideosFromMovie.observe(viewLifecycleOwner, {
            if (it.results[0].site == "YouTube")
                initVideo(it)
            else hideVideoViews()
        }, {
            it.printStackTrace()
        })
    }

    private fun hideVideoViews() {
        binding.video.visibility = View.GONE
        binding.videoTitle.visibility = View.GONE
    }

    private fun initVideo(it: VideosFromMovie) {
        runCatching {
            lifecycle.addObserver(binding.video)
            binding.videoTitle.text = it.results[0].name
            binding.video.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                override fun onReady(youTubePlayer: YouTubePlayer) {
                    youTubePlayer.loadVideo(it.results[0].key, 0f)
                    binding.videoTitle.visibility = View.VISIBLE
                    binding.video.visibility = View.VISIBLE
                }
            })
        }.onFailure {
            it.printStackTrace()
        }
    }

    private fun initViewDetailed(movieDetails: MovieDetails) {
        with(binding) {
            movieTitle.text = movieDetails.title
            description.text = movieDetails.overview
            genres.text = setGenres(movieDetails.genres)
            homePage.text = movieDetails.homepage
            if (movieDetails.tagline.isEmpty())
                tagLine.visibility = View.GONE
            else tagLine.text = movieDetails.tagline
            Glide.with(requireContext())
                .load(BuildConfig.LOAD_IMAGE_ORIGINAL + movieDetails.posterPath)
                .into(poster)
            loading.visibility = View.GONE
            poster.visibility = View.VISIBLE
            divider.visibility = View.VISIBLE
            genresLabel.visibility = View.VISIBLE
        }
    }

    private fun setGenres(genres: ArrayList<Genres>): String {
        var genresList = ""
        genres.forEach {
            genresList += it.name + ", "
        }
        return genresList.dropLast(2)
    }
}