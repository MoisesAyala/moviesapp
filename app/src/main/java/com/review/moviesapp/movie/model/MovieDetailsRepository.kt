package com.review.moviesapp.movie.model

import com.review.moviesapp.movie.model.response.MovieDetails
import com.review.moviesapp.movie.model.response.VideosFromMovie

interface MovieDetailsRepository {
    suspend fun callMovieDetails(movieId: Int): MovieDetails
    suspend fun callVideosFromMovie(movieId: Int): VideosFromMovie
}