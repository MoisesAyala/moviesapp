package com.review.moviesapp.movie.extension

import com.review.moviesapp.home.model.response.MostPopular
import com.review.moviesapp.home.model.response.PlayingNow
import com.review.moviesapp.movie.model.response.MovieDetails
import com.review.moviesapp.movie.model.response.VideosFromMovie

fun MovieDetails.avoidErrorNullData(): MovieDetails {
    return MovieDetails(
        adult = this.adult,
        backdropPath = this.backdropPath,
        belongsToCollection = this.belongsToCollection,
        budget = this.budget,
        genres = this.genres,
        homepage = this.homepage,
        id = this.id,
        imdbId = this.imdbId,
        originalLanguage = this.originalLanguage,
        originalTitle = this.originalTitle,
        overview = this.overview,
        popularity = this.popularity,
        posterPath = this.posterPath,
        productionCompanies = this.productionCompanies,
        productionCountries = this.productionCountries,
        releaseDate = this.releaseDate,
        revenue = this.revenue,
        runtime = this.runtime,
        spokenLanguages = this.spokenLanguages,
        status = this.status,
        tagline = this.tagline,
        title = this.title,
        video = this.video,
        voteAverage = this.voteAverage,
        voteCount = this.voteCount
    )
}

fun VideosFromMovie.avoidErrorNullData(): VideosFromMovie {
    return VideosFromMovie(
        id = this.id,
        results = this.results
    )
}