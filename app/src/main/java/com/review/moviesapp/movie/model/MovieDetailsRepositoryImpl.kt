package com.review.moviesapp.movie.model

import com.review.moviesapp.BuildConfig
import com.review.moviesapp.movie.extension.avoidErrorNullData
import com.review.moviesapp.movie.model.response.MovieDetails
import com.review.moviesapp.movie.model.response.VideosFromMovie
import com.review.moviesapp.retrofit.ApiAdapter

class MovieDetailsRepositoryImpl : MovieDetailsRepository {

    private val apiKey = "1fb75181acbca031bbb3237794468c44"
    private val locale = "es-MX"

    override suspend fun callMovieDetails(movieId: Int): MovieDetails {
        val api = ApiAdapter(BuildConfig.MOVIE_DB_API)
        val result = api.clientService().callMovieDetails(movieId, apiKey, locale)
        return result.avoidErrorNullData()
    }

    override suspend fun callVideosFromMovie(movieId: Int): VideosFromMovie {
        val api = ApiAdapter(BuildConfig.MOVIE_DB_API)
        val result = api.clientService().callVideosFromMovie(movieId, apiKey, locale)
        return result.avoidErrorNullData()
    }
}